<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Index.php</title>
    <link rel="stylesheet" href="styles.css">
    <link href='https://fonts.googleapis.com/css?family=Anonymous Pro' rel='stylesheet'>
  </head>
  <body >
    <nav>
      <h1>∫(PHP Login) </h1>
      <div class="navbar">
        <a class="active" href="index.php"><i class="fa fa-fw fa-home"></i> Inicio</a>
        <a href="#"><i class="fa fa-fw fa-search"></i> Buscar</a>
        <a href="signup.php"><i class="fa fa-fw fa-envelope"></i> Registro</a>
        <a href="login.php"><i class="fa fa-fw fa-user"></i> Login</a>
        <a href="file.php">Subir archivo</a>
      </div>
    </nav>
